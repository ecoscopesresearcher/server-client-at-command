#include <SoftwareSerial.h>
#include "Timer.h" //Download lib external http://playground.arduino.cc/Code/Timer
#include <EEPROM.h>
#include <Arduino.h> 
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 256
char buffer[BUFFER_SIZE];
#define DEBUG true
#define LEDTEST 13
/*
holding for design API server
char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/
Timer t;
bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false;
bool gleds[2];
bool gCallServer = false,gCalledServer=true;
String GetAPResponceArray(const int timeout, const char* ack,int len = 0);

void setup() {
  #if DEBUG
  Serial.begin(9600);
  #endif
  //Led test
  pinMode(LEDTEST, OUTPUT);
  gleds[0] = true;
   gleds[1] = false;
  digitalWrite(LEDTEST, gleds[0]);
  //AT+CIOBAUD=57600
  esp8266.begin(57600); 
  ResetConfigBase();
  
 
}

void loop() {
  //unsigned long int time = millis();
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else  if(esp8266.available()) // check if the esp is sending a message 
  {  
    
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    #if DEBUG
    Serial.println(buffer);
    #endif
   
   return;
     if(strncmp(buffer, "+IPD,", 5)==0)
     {
       Serial.println("--IPD--");
        int connectionId,packet_len;
        char *pb; 
        String webpage="NF"; 
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);
        String response = "";
        long int time = millis();
        int timeout = 30000;
        while( (time+timeout) > millis())
        {
            while(esp8266.available())
            {
              // The esp has data so display its output to the serial window 
              char c = esp8266.read(); // read the next character.
              response+=c;
              Serial.print(c);
            }  
        }
        //Serial.println(response);
     }
  }
   
  #if DEBUG
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    } 
    if(command.substring(0,2)=="CA")
    {
       CallServer(); 
    }
    if(command.substring(0,2)=="CB"){
       CallServer2();
    }else  esp8266.println(command); // send the read character to the esp8266 
    if(command.substring(0,2)=="AT")
    {
       
    }  
  }
  #endif
}

void CallServer2()
{
            gCallServer=false;  
            gCalledServer  =true;    
            //String cmd = "AT+CIPSTART=1,\"TCP\",\"184.106.153.149\",80";
            //String cmd = "AT+CIPSTART=1,\"TCP\",\"api.thingspeak.com\",80";
            String cmd = "AT+CIPSTART=1,\"TCP\",\"192.168.1.38\",8080";
            #if DEBUG
            Serial.println(F("---Connecting-2--"));
            #endif
            esp8266.println(cmd);
            gResult = false;
            GetAPResponceArray(30000,"AL",2);
  
            if(!gResult)
            {
             #if DEBUG
             Serial.println(F("---Connection fail---"));
             #endif
             return;
            }

           /*client.print("POST /update HTTP/1.1\n"); 
           client.print("Host: api.thingspeak.com\n"); 
           client.print("Connection: close\n"); 
           client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n"); 
           client.print("Content-Type: application/x-www-form-urlencoded\n"); 
           client.print("Content-Length: "); */
            String field1="field1=80";
            long randNumber = random(0, 100);
            String api_key="PNAFM98LOSZOG9S8";
            
           /* String http_req="POST /update HTTP/1.1\r\n";
            http_req+="Host: api.thingspeak.com\r\n";
            http_req+="Connection: close\r\n";
            http_req+="X-THINGSPEAKAPIKEY: "+api_key+"\r\n";
            http_req+="Content-Type: application/x-www-form-urlencoded\r\n";
            http_req+="Content-Length: ";
            http_req+=field1.length();
            http_req += "\r\n\r\n";
            http_req += field1;
            http_req +="\r\n\r\n";
            String cmd2 = "AT+CIPSEND=1,";
            cmd2 +=String(http_req.length()+field1.length());
            Serial.println(http_req);
            gResult = false;
            esp8266.println(cmd2);*/
            //String http_req="GET /update?key=J4P1LICZI6ZF0YWG&field1=100\r\n";
            
            String http_req="GET /i.php\r\n";
            String cmd2 = "AT+CIPSEND=1,";
            cmd2 +=String(http_req.length());
            Serial.println(cmd2);
            Serial.println(http_req);
            gResult = false;
            esp8266.println(cmd2);
            GetAPResponceArray(30000,">",1);
           if(gResult)
            {    
              #if DEBUG
              Serial.println(F("---sending---"));
              #endif
              esp8266.println(http_req);
              int timeout = 10000;
              long int time = millis();   
               while(esp8266.available()){
                  esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
                  #if DEBUG
                  Serial.println(buffer);
                  #endif
                  
                    if((time+timeout) < millis())
                     {
                       
                      return;        
                     }
                  }
            }
         
            /*String closeCommand = "AT+CIPCLOSE=1"; 
            closeCommand+="\r\n";
            esp8266.println(closeCommand);     
            GetAPResponceArray(500,"OK",2);*/
}

void CallServer()
{
            gCallServer=false;  
            gCalledServer  =true;    
            //String cmd = "AT+CIPSTART=1,\"TCP\",\"184.106.153.149\",80";
            //String cmd = "AT+CIPSTART=1,\"TCP\",\"api.thingspeak.com\",80";
            String cmd = "AT+CIPSTART=1,\"TCP\",\"192.168.1.38\",8080";
            #if DEBUG
            Serial.println(F("---Connecting---"));
            #endif
            esp8266.println(cmd);
            gResult = false;
            GetAPResponceArray(30000,"AL",2);
  
            if(!gResult)
            {
             #if DEBUG
             Serial.println(F("---Connection fail---"));
             #endif
             return;
            }

           /*client.print("POST /update HTTP/1.1\n"); 
           client.print("Host: api.thingspeak.com\n"); 
           client.print("Connection: close\n"); 
           client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n"); 
           client.print("Content-Type: application/x-www-form-urlencoded\n"); 
           client.print("Content-Length: "); */
            String field1="field1=80";
            long randNumber = random(0, 100);
            String api_key="PNAFM98LOSZOG9S8";
            
           /* String http_req="POST /update HTTP/1.1\r\n";
            http_req+="Host: api.thingspeak.com\r\n";
            http_req+="Connection: close\r\n";
            http_req+="X-THINGSPEAKAPIKEY: "+api_key+"\r\n";
            http_req+="Content-Type: application/x-www-form-urlencoded\r\n";
            http_req+="Content-Length: ";
            http_req+=field1.length();
            http_req += "\r\n\r\n";
            http_req += field1;
            http_req +="\r\n\r\n";
            String cmd2 = "AT+CIPSEND=1,";
            cmd2 +=String(http_req.length()+field1.length());
            Serial.println(http_req);
            gResult = false;
            esp8266.println(cmd2);*/
            //String http_req="GET /update?key=J4P1LICZI6ZF0YWG&field1=100\r\n";
            
            String http_req="GET /input/post.json?json={plug1:";
            http_req+=String(randNumber);
            http_req+=",plug2:";
            randNumber = random(0, 100);
            http_req+=String(randNumber);
            http_req+="}&node=1&apikey=204be581ffbc8e34af79bf25b2d638fb\r\n";
            String cmd2 = "AT+CIPSEND=1,";
            cmd2 +=String(http_req.length());
            Serial.println(cmd2);
            Serial.println(http_req);
            gResult = false;
            esp8266.println(cmd2);
            GetAPResponceArray(30000,">",1);
           if(gResult)
            {    
              #if DEBUG
              Serial.println(F("---sending---"));
              #endif
              esp8266.println(http_req);
              int timeout = 10000;
              long int time = millis();   
               while(esp8266.available()){
                  esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
                  #if DEBUG
                  Serial.println(buffer);
                  #endif
                  
                    if((time+timeout) < millis())
                     {
                       
                      return;        
                     }
                  }
            }
         
            /*String closeCommand = "AT+CIPCLOSE=1"; 
            closeCommand+="\r\n";
            esp8266.println(closeCommand);     
            GetAPResponceArray(500,"OK",2);*/

}
String GetAPResponceArray(const int timeout, const char* ack,int len )
{   
  bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             #if DEBUG
             Serial.println(buffer);
             #endif
             content=content+","+buffer+"";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                #if DEBUG
                Serial.println("---ack---");
                #endif
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0||strncmp(buffer, "FAIL",4)==0){
                endCallBack= true;
                gResult =true; 
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",TO";  
                endCallBack = true;            
             }
   }
   return content;
}

void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() >0 ) {
         delay(10);
         esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}

     

