
#include <SoftwareSerial.h>
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
bool WIFINEEDREST = false;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  esp8266.begin(9600); 
  pinMode(13, OUTPUT);
  ResetConfigBase();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }else if(esp8266.available()) // check if the esp is sending a message 
  { 
    clearBuffer();
    Serial.println("---available---");
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    Serial.println(buffer);
     if(strncmp(buffer, "+IPD,", 5)==0){
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);      
        char *pb;    
        Serial.println("---REQ---");
                
        pb = buffer+5;
        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+CIFSR\0";
        int code = (int)*pb;
       
        clearWifBuffer();
        String  webpage ;//= GetAPResponceArray("AT+CIFSR",10000,1000) ; 
      
        esp8266.println(ComEsp8266);
        webpage = "{rsp:["+GetAPResponceArray(10000)+"]}";//
         Serial.println("------------Web-------------");
         Serial.println(webpage);
         Serial.println("---------------------------");
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage.length();
        HeaderContent += "\r\n\r\n"+webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        Serial.println("---cmd cip---");
        delay(1000); 
        Serial.println(GetAPResponceArray(cipSend,500,0));
        delay(1000); 
        Serial.println("---body---");
        Serial.println(GetAPResponceArray(HeaderContent,500,0));
        delay(1000); 
        Serial.println("---close---");
        CloseConnection(connectionId);
        Serial.println("---END-CLOSE---");
     }
  }
    if(Serial.available())
    {
      delay(500);     
      String command="";
      while(Serial.available()) // read the command character by character
      {
        //read one character
        command+=(char)Serial.read();
      }     
      if(command.substring(0,2)=="AT")
      {
        esp8266.println(command); // send the read character to the esp8266
        //esp8266.flush();
      }  
    }
}

void  CloseConnection(int connectionId)
{
      String closeCommand = "AT+CIPCLOSE="; 
      closeCommand+=connectionId; // append connection id
      closeCommand+="\r\n";
          
        Serial.println(GetAPResponceArray(closeCommand,1000,0));      
}
String GetAPResponceArray(String command, const int timeout,const int delayCommand)
{
   esp8266.print(command);
    delay(delayCommand);
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       content=content+",'"+buffer+"'";  
       Serial.println(buffer);
       if(strncmp(buffer, "busy", 4)==0)
       {
          WIFINEEDREST = true;
          Serial.println("---busy---");
          endCallBack= true;
       }else if(strncmp(buffer, "reay", 4)==0||strncmp(buffer, "MUX=0", 5)==0){
          WIFINEEDREST = true;
          Serial.println("---reay---");
          endCallBack= true;
       }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
          Serial.println("---list---");
          endCallBack= true;
       }else if((time+timeout) < millis())
       {
          content=content+",'TO'";  
          endCallBack = true;  
          //WIFINEEDREST = true;
       }
   }
   return content;
}
 String sendCommandAndWaiting(String command, const int timeout)
{
    String response = "";    
    esp8266.print(command); // send the read character to the esp8266   
    //esp8266.flush();
    long int time = millis();    
    while( (time+timeout) > millis())
    {
     
      while(esp8266.available())
      {        
        // The esp has data so display its output to the serial window 
        char c = esp8266.read(); // read the next character.
        response+=c;
      }  
    }   
    return response;
}



void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
   
    esp8266.println("AT+CWMODE=3");
    //esp8266.flush();
    delay(500);  
    esp8266.println("AT+CIFSR");
    //esp8266.flush();
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    //esp8266.flush();
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");
    //esp8266.flush();
      
}

void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}
void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
String GetAPResponceArray(const int timeout)
{
   
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       
       
       content=content+",'"+buffer+"'";  
       Serial.println(buffer);
       if(strncmp(buffer, "busy", 4)==0)
       {
          WIFINEEDREST = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "reay", 4)==0){
          WIFINEEDREST = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
          Serial.println("---list---");
          endCallBack= true;
       }else if((time+timeout) < millis())
       {
          content=content+",'TO'";  
          endCallBack = true;  
          //WIFINEEDREST = true;
       }
   }
   return content;
}
