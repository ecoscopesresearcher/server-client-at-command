#include <SoftwareSerial.h>

SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
char PlugName[15];
char ServerUrl[20];
char APIKEY[5];
unsigned long int timeIntervalChecking = 30000; 
unsigned long int previousMillis;
bool WIFINEEDREST = false;
bool gResult = false;
String http_req ;
bool hasReq = false;
String GetAPResponceArray(String command,const int timeout, const char* ack,int len = 0);
bool RequireRestfulCommand(String command,int timeout,const char* ack,int len);

void setup() {
  Serial.begin(9600);
  esp8266.begin(9600); 
  pinMode(13, OUTPUT);
  //digitalWrite(13, open);
  ResetConfigBase();
  previousMillis =millis();
  
  http_req = "GET /status/4 HTTP/1.1\r\n";
  http_req+=" Host: 192.168.1.35\r\n\r\n";  //192.168.1.36: 16290
}

void loop() {
  // put your main code here, to run repeatedly:
  
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else  if(esp8266.available()||hasReq) // check if the esp is sending a message 
  {  
    if(hasReq){
      Serial.println("---hasReq---");
      hasReq = false;
     
    }else{
      clearBuffer();   
      Serial.println("---available---");
      esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
     
    }
     Serial.println(buffer);
    if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0||strncmp(buffer, "+CIPMUX:0", 9)==0)
    { 
      WIFINEEDREST =true;
      //ResetConfigBase();
    }else if(strncmp(buffer, "+IPD,", 5)==0){   
        delay(500);
        clearWifBuffer();   
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
        char *pb;    
        Serial.println("---REQ---");
        String webpage="NF";         
        pb = buffer+5;        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+";
        int code = (int)*pb;    
        clearWifBuffer();    
        if(64==code)//@ command
        {
          Serial.println("--GETCMD--");    
          //
          webpage = "{r:['C','"+Command(++pb)+"']}"; 
          
        }
        else if(code>66 ){         
              Serial.print("--QUEUEY--");
              
              switch(code)
              {
                case 71: //G List AP
                ComEsp8266+="CWLAP";
                break;
                case 67://C Check current AP
                ComEsp8266+="CWJAP?";
                break;
                case 73://I Get IP
                ComEsp8266+="CIFSR";
                break;
              }
              Serial.println(ComEsp8266);
              esp8266.println(ComEsp8266+"\0");
              webpage = "{r:['Q',"+GetAPResponceArray(10000)+"]}";
        }
        else if(78==code){
        //N Get name Itseft.       
        }
        else if( 67>code && 64<code) //A Join AP,B set name passs
        {
            clearWifBuffer();
            Serial.println("--A,B--");  
            String value[2]; 
            int index = 0;
            
            while(*pb!=61) pb++;  
            Serial.println( *pb);
            pb++;
            
            while(*pb > 45 && *pb<122 && index <2)
            {
              value[index]+=*pb;
             
              if(*(pb+1)==44)
              {               
                index++;
                pb++;
              }
               pb++;
            }
           if(65==code)//A Join AP
           {
                 ComEsp8266 += "CWJAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
              
           }else if(66== code)//Set name ap.
           {
                 ComEsp8266 += "CWSAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
                 ComEsp8266 +=",10,0";
           }  
           Serial.println(ComEsp8266);
           esp8266.println(ComEsp8266+"\0");
           webpage = "{r:['W',"+GetAPResponceArray(10000)+"]}";
        }   
       /*clearWifBuffer();    
       String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        //HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage.length();
        HeaderContent += "\r\n\r\n"+webpage;
         Serial.println("------------Web-------------");
         Serial.println(HeaderContent);
         Serial.println("---------------------------");
          String cipSend = "AT+CIPSEND=";
         
           cipSend += connectionId;
           cipSend += ",";
           cipSend +=HeaderContent.length();
           cipSend +="\r\n";
           
         
          
           GetAPResponceArray(cipSend,1000,">",1) ;
            Serial.println("--------HEADER----------");
           GetAPResponceArray(HeaderContent,1500,"OK",2) ;
          
           String closeCommand = "AT+CIPCLOSE="; 
           closeCommand+=connectionId; // append connection id
           closeCommand+="\r\n";         
           Serial.println("--------CLOSE----------");
           GetAPResponceArray(closeCommand,1500,"OK",2) ;
            Serial.println("---------------------------");*/
         clearWifBuffer();    
         Responce(connectionId,&webpage);        
    }
  }
    long currentMillis=millis();
    if(currentMillis-previousMillis>timeIntervalChecking){ //Interval
     Serial.println("---checking---");
     previousMillis =currentMillis;
     esp8266.println("AT+CIPMUX?");
     }
    
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }
}
String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             Serial.println(buffer);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;            
             }
   }
   return content;
}
void Responce(const int connectionId, String *webpage) 
{
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage->length();
        HeaderContent += "\r\n\r\n"+*webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        Serial.println("---cmd cip---");
        Serial.println(GetAPResponceArray(cipSend,1000,">",1));            
        Serial.println("---body---");
        Serial.println(GetAPResponceArray(HeaderContent,1500,"OK",2));  
        Serial.println("---close---");
        String closeCommand = "AT+CIPCLOSE="; 
        closeCommand+=connectionId; // append connection id
        closeCommand+="\r\n";
         
        Serial.println(closeCommand);
        Serial.println(GetAPResponceArray(closeCommand,500,"OK",2));    
        Serial.println("---END-CLOSE---");
}

String Command(char *cmd)
{  
   Serial.println(*cmd);
   String resp;
   int code = (int)*cmd;
   if(code > 63 && code<71 ) // set command A - F
   {
      int count = 0;
      String value; 
      cmd++;
      while(45<*cmd&&*cmd<123)
      {     
        value+=*cmd;
        cmd++;
        count++;
      }
      Serial.println("V="+value);
      if(code == 65) // A 
      {
        // Set server url
         Serial.println(65);Serial.println(count);     
         if(count<21)
          {
             value.toCharArray(ServerUrl,count+1);
              Serial.println(ServerUrl);
              return "SU";     
          }
           return "FC";  
      }
      else if(code == 66) //B
      {
        Serial.println(66);Serial.println(count);
          //Set API sKey
          if(count<6)
          {
              value.toCharArray(APIKEY,count+1);
             Serial.println(APIKEY);
             return "SU";   
          }
         return "FC";   
             
      }
   }else if(code > 70 && code<79){//G-N
        if(code==71) //G Get url ,APK , Name
        {   
          
          resp =  ServerUrl;  
          resp += ",";
          resp +=APIKEY; 
          return resp;
        }
      }
   
   return "NF";
}




void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}


 




String GetAPResponceArray(const int timeout)
{
   
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       
       
       content=content+",'"+buffer+"'";  
       Serial.println(buffer);
       if(strncmp(buffer, "busy", 4)==0)
       {
          WIFINEEDREST = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "reay", 4)==0){
          WIFINEEDREST = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
          Serial.println("---list---");
          endCallBack= true;
       }else if((time+timeout) < millis())
       {
          content=content+",'TO'";  
          endCallBack = true;  
          //WIFINEEDREST = true;
       }
   }
   return content;
}

void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
