#include <SoftwareSerial.h>
SoftwareSerial esp8266(2,3); 
int forktest=10;
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
bool open =false;
void createForTestfork()
{

}
int timeIntervalChecking = 60000; 
bool WIFINEEDREST = false,WIFIRESET=false;
#define RESETWIFI_PIN 12
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  esp8266.begin(9600); 
  pinMode(13, OUTPUT);
  pinMode(RESETWIFI_PIN, OUTPUT);
  digitalWrite(13, open);
  esp8266.println("AT+CWMODE?");
  digitalWrite(RESETWIFI_PIN, HIGH);
  if(esp8266.find("+CWMODE:3"))
  {
      Serial.println("Not ot setting");
      //PlugName = GetPlugName();
  }else{
       ResetConfigBase();
  }  
  
}

void loop() {
  
 
  // put your main code here, to run repeatedly:
  if(esp8266.available()) // check if the esp is sending a message 
  {  
      if(WIFINEEDREST){
          //CUT power wifi
          Serial.println("---CUT power wifi---");
          WIFIRESET = true;
          WIFINEEDREST = false;
       return;  
      }
      clearBuffer();
      Serial.println("---available---");
      esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
      Serial.println(buffer);
      
      if(strncmp(buffer, "ready", 5)==0||WIFIRESET)
      { 
         //case wifi reset itseft. 
         ResetConfigBase();
         WIFIRESET = false;
         
      }else if(strncmp(buffer, "+IPD,", 5)==0){
        
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);
        
        char *pb;    
        Serial.println("---REQ---");
        String webpage="NF"; 
        
        pb = buffer+5;
        
        while(*pb!=':') pb++;
        pb++;
        
        if(strncmp(pb, "GET /CMD", 8) == 0){
          
          Serial.println("--GETCMD--");         
          webpage = "{rsp:['CMD']}"; 
          
        }else if(strncmp(pb, "GET /GAP", 8) == 0){
          
          Serial.println("--GETGAP--");
          delay(100); 
          esp8266.println("AT+CWLAP");
          webpage = "{rsp:['CWLAP'"+GetAPResponceArray(10000)+"']}";
          
        }else if(strncmp(pb, "GET /SAP", 8)==0){//Set Parameters of Access Point
          String setParameter="";
          String ssid,password;
          Serial.print ("--GETSAP--=");                  
          while(*pb!='=') pb++;                   
          Serial.print(*pb);         
          pb++;
          while(*pb!=',')
          {
             char c = *pb;
             ssid+=c;
             pb++;
          }
          pb++;
          while(*pb!='?') {
              char c = *pb; // read the next character.
              password+=c;
              pb++;
              Serial.print(*pb); 
          }
          setParameter = '"'+ssid+'"'+','+'"'+password+'"';                  
          clearWifBuffer();
          Serial.println("AT+CWJAP="+setParameter);
          delay(100);
          esp8266.println("AT+CWJAP="+setParameter);
          webpage = "{rsp:["+GetAPResponceArray(100000)+"]}";
          Serial.println("------AT+CWJAP------");
           //webpage = "{rsp:['CWSAP']}";
        }else if(strncmp(pb, "GET /CAP", 8)==0){ //Check AP
          clearWifBuffer();
          Serial.println("AT+CWJAP?");
          delay(100);
          esp8266.println("AT+CWJAP?");
          webpage = "{rsp:["+GetAPResponceArray(100000)+"]}";
          Serial.println("------AT+CWSAP------");
        }else  if(strncmp(pb, "GET /GIP", 8)==0){//GET IP
        //AT+CIFSR
          clearWifBuffer();
          Serial.println("AT+CIFSR");
          delay(100);
          esp8266.println("AT+CIFSR");
          webpage = "{rsp:["+GetAPResponceArray(10000)+"]}";
          Serial.println("------AT+CIFSR------");
        }else if(strncmp(pb, "GET /CNA", 8)==0){//Check seft name ap.
            //
          Serial.println("GET NAME");

          webpage = "{rsp:['WIFINAME']}";
        }else if(strncmp(pb, "GET /SNA", 8)==0){//Set name ap.
          
          String setParameter="";
          String ssid,password;
          while(*pb!='=') pb++;                   
          Serial.print(*pb);         
          pb++;
          while(*pb!=',')
          {
             char c = *pb;
             ssid+=c;
             pb++;
          }
          pb++;
          while(*pb!='?') {
              char c = *pb; // read the next character.
              password+=c;
              pb++;
              Serial.print(*pb); 
          }
         
          setParameter = '"'+ssid+'"'+','+'"'+password+'"';   
          clearWifBuffer();
          Serial.println("AT+CWSAP=xxxx,xxx");
          delay(100);
          esp8266.println("AT+CWSAP="+setParameter);
          esp8266.flush();
          webpage = "{rsp:["+GetAPResponceArray(10000)+"]}";
        }
        
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage.length();
        HeaderContent += "\r\n\r\n"+webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        Serial.println("---cmd cip---");
        Serial.println(cipSend);
        Serial.println(HeaderContent);
        Serial.println("---end cip---");
        delay(100); 
        sendCommandAndWaiting(cipSend,1000);
        delay(100); 
        sendCommandAndWaiting(HeaderContent,2000);
        delay(100); 
        Serial.println("----CLOSE----");
        CloseConnection(connectionId);
        Serial.println("---END-CLOSE---");
      }
      
  }
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }    

    if(command.substring(0,2)=="AT")
    {
      esp8266.println(command); // send the read character to the esp8266
    }  
  }
}
String GetPlugName()
{
  esp8266.println("AT+CWSAP?");
  //sendCommandAndWaiting("AT+CWSAP?",10000);
  delay(1000);
  char bufferName[BUFFER_SIZE]; 
  String plugName;
  esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
  char *pb;
  pb = buffer;
  pb+=8;
  while(*pb!='"') {
              char c = *pb; // read the next character.
              plugName+=c;
              pb++;    
  }
  Serial.print("plugname=");
   Serial.println(plugName);
  
  return plugName;
}
String GetAPResponceArray(const int timeout)
{
   
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       
       
       content=content+",'"+buffer+"'";  
       Serial.println(buffer);
       if(strncmp(buffer, "busy", 4)==0)
       {
          WIFINEEDREST = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "reay", 4)==0){
          WIFIRESET = true;
          Serial.println("---list---");
          endCallBack= true;
       }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
          Serial.println("---list---");
          endCallBack= true;
       }else if((time+timeout) < millis())
       {
          content=content+",'TO'";  
          endCallBack = true;  
          WIFINEEDREST = true;
       }
   }
   return content;
}

void ResetConfigBase()
{
  delay(100); 
    esp8266.println("AT+CWMODE=3");
    delay(100); 
    delay(100); 
    esp8266.println("AT+CIFSR");
    delay(100);   
    esp8266.println("AT+CIPMUX=1");
    delay(100); 
    esp8266.println("AT+CIPSERVER=1,80");
    delay(100);   
    //PlugName = GetPlugName();
}

void  CloseConnection(int connectionId)
{
     String closeCommand = "AT+CIPCLOSE="; 
      closeCommand+=connectionId; // append connection id
      closeCommand+="\r\n";
      String resp;      
      sendCommandAndWaiting(closeCommand,1000);      
}
String sendCommandAndWaiting(String command, const int timeout)
{
    String response = "";    
    esp8266.print(command); // send the read character to the esp8266   
    long int time = millis();    
    while( (time+timeout) > millis())
    {
      while(esp8266.available())
      {        
        // The esp has data so display its output to the serial window 
        char c = esp8266.read(); // read the next character.
        response+=c;
      }  
    }
   
    return response;
}
 void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}
