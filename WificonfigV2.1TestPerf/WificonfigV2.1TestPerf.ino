#include <SoftwareSerial.h>
#include "Timer.h"
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
#define debug false
char buffer[BUFFER_SIZE];
/*char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/

bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false;
Timer t;
String GetAPResponceArray(const int timeout, const char* ack,int len = 0);
void setup() {
  #if DEBUG
  Serial.begin(9600);
  #endif
  esp8266.begin(57600); 
  pinMode(13, OUTPUT);
  //esp8266.println("AT+CIOBAUD=57600");
  //digitalWrite(13, open);
  ResetConfigBase();
  
  t.every(30000, Interval);
}

void loop() {
  //unsigned long int time = millis();
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else  if(esp8266.available()) // check if the esp is sending a message 
  {  
    //clearBuffer();
   
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
   #if DEBUG
    Serial.println(buffer);
   #endif 
     if(strncmp(buffer, "+IPD,", 5)==0){
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
        char *pb;    
        //Serial.println("---REQ---");
        String webpage="NF";         
        pb = buffer+5;        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        //Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+";
        int code = (int)*pb;        
        if(64==code)//@ command
        {
          //Serial.println("--GETCMD--");    
          
          webpage = "C";//+Command(++pb); 
          //esp8266.flush();
          //Serial.println("--clear--");
          clearWifBuffer();
        }
         //Serial.println("--send--");
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        //HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage.length();
        HeaderContent += "\r\n\r\n"+webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        GetAPResponceArray(cipSend,500,">",1);  
        GetAPResponceArray(HeaderContent,1000,"SE",2);    
        //Serial.println("---close---");
        String closeCommand = "AT+CIPCLOSE="; 
        closeCommand+=connectionId; // append connection id
        closeCommand+="\r\n";
        //esp8266.println(closeCommand+"\0");
        GetAPResponceArray(closeCommand,500,"OK",2);    
    }
  }
  

    
  /*if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }*/
}



String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       //clearBuffer();
       //delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             //Serial.println(buffer);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                //Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;            
             }
   }
   return content;
}

void Interval()
{
  livingInterval = true;
 
}
void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
          delay(10);
          esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         delay(10);
         buffer[i]=0;
       }
}
