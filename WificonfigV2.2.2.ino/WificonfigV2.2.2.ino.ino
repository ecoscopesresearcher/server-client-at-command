#include <SoftwareSerial.h>
#include "Timer.h" //Download lib external http://playground.arduino.cc/Code/Timer
#include <EEPROM.h>
#include <Arduino.h> 
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
#define DEBUG false
#define LEDTEST 13
/*
holding for design API server
char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/
Timer t;
bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false;
bool gleds[2];
bool gCallServer = false,gCalledServer=true;
String GetAPResponceArray(const int timeout, const char* ack,int len = 0);

void setup() {
  #if DEBUG
  Serial.begin(9600);
  #endif
  //Led test
  pinMode(LEDTEST, OUTPUT);
  gleds[0] = true;
   gleds[1] = false;
  digitalWrite(LEDTEST, gleds[0]);
  
  esp8266.begin(57600); 
  ResetConfigBase();
  
  t.every(10000, Interval);
}

void loop() {
  //unsigned long int time = millis();
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else  if(esp8266.available()) // check if the esp is sending a message 
  {  

    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    #if DEBUG
    Serial.println(buffer);
    #endif
     if(strncmp(buffer, "+IPD,", 5)==0||gCallServer){
       
        
        int connectionId,packet_len;
        char *pb; 
        String webpage="NF"; 
        if(gCallServer)
        {
            gCallServer=false;  
            gCalledServer  =true;    
            String cmd = "AT+CIPSTART=4,\"TCP\",\"192.168.1.35\",8080";
            #if DEBUG
            Serial.println("---Connecting---");
            #endif
            esp8266.println(cmd);
            gResult = false;
            GetAPResponceArray(30000,"AL",2);
  
            if(!gResult)
            {
             #if DEBUG
             Serial.println("---Connection fail---");
             #endif
             return;
            }
            //http://192.168.1.35:8080/input/post.json?json={plug1:20,plug2:51}&node=1
            String http_req = "GET /input/post.json?json={plug1:20,plug2:51}&node=1&apikey=204be581ffbc8e34af79bf25b2d638fb HTTP/1.1\r\nHost: 192.168.1.35:8080\r\n\r\n";
            String cmd2 = "AT+CIPSEND=4,";
            cmd2 = cmd2 + http_req.length();
            gResult = false;
            esp8266.println(cmd2);
            GetAPResponceArray(30000,">",1);
           if(gResult)
            {    
              #if DEBUG
              Serial.println("---sending---");
              #endif
              esp8266.println(http_req);
              GetAPResponceArray(30000,"OK",2);
            }
            #if DEBUG
            Serial.println("---close---");
            #endif
            String closeCommand = "AT+CIPCLOSE=4"; 
            closeCommand+="\r\n";
            esp8266.println(closeCommand);     
            GetAPResponceArray(500,"OK",2);   

        }else{
            sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
            
            #if DEBUG   
            Serial.println("---REQ---");
            #endif
                    
            pb = buffer+5;        
            while(*pb!=':') pb++;
            pb++;
            pb+=5;
        }
        #if DEBUG  
        Serial.print("ASCII=");Serial.println((int)*pb);
        #endif
        String ComEsp8266 ="AT+";
        int code = (int)*pb;        
        if(64==code)//@ command
        {
          webpage = "C,"+Command(++pb); 
          //clearWifBuffer();
        }else if(code>67 ){         
              
              switch(code)
              {             
                //We do not need it.
                case 71: //G List AP
                ComEsp8266+="CWLAP";
                break;
                case 68://D Check current AP
                ComEsp8266+="CWJAP?";
                break;
                
                case 73://I Get IP
                ComEsp8266+="CIFSR";
                break;
                
                case 74://J Get Plug Name
                ComEsp8266+="CWSAP?";
                break;
              }
              clearWifBuffer();
              esp8266.println(ComEsp8266);
              webpage = "Q"+GetAPResponceArray(10000,"OK",2);
            
             
        }
        else if(78==code){
        //N Get name Itseft.       
        }
        else if( 68>code && 64<code) //A Join AP,B set name passs C test 
        {
            
           
            String value[2]; 
            int index = 0;
            
            while(*pb!=61){ 
            pb++ ;
            #if DEBUG           
            Serial.print(*pb);
            #endif  
            }  
           
            pb++;
           
            while((int)*pb > 45 && (int)*pb<122 && index <2)
            {
              value[index]+=*pb;
             
              if(*(pb+1)==',')
              {               
                index++;
                pb++;
              }
               pb++;
            }
            #if DEBUG
            Serial.println(value[0]);
            Serial.println(value[1]);
            #endif
           if(65==code)//A Join AP
           {
                 ComEsp8266 += "CWJAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
              
           }else if(66== code)//B Set name ap.
           {
                 ComEsp8266 += "CWSAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
                 ComEsp8266 +=",10,0";
           }
           else if(67== code)//C Test.
           {
                
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
                
           }  
           
           clearWifBuffer();
           esp8266.println(ComEsp8266+"\0");
           webpage = "S"+GetAPResponceArray(30000,"OK",2);
        }   
        if(gCalledServer)
        {
        
        }else{
         #if DEBUG
         Serial.println("------------Web-------------");
         Serial.println(webpage);
         Serial.println("---------------------------");
         #endif
         
         Responce(connectionId,&webpage);       
        } 
    }
  }
   else  if(livingInterval)
  {
   
    esp8266.println("AT+CIPMUX?");
    livingInterval = false;
  }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0||strncmp(buffer, "+CIPMUX:0", 9)==0)
  { 
      WIFINEEDREST =true;
  }
  else t.update();
  #if DEBUG
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }
  #endif
}
String GetAPResponceArray(const int timeout, const char* ack,int len )
{   
  bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             #if DEBUG
             Serial.println(buffer);
             #endif
             content=content+","+buffer+"";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                #if DEBUG
                Serial.println("---ack---");
                #endif
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0||strncmp(buffer, "FAIL",4)==0){
                endCallBack= true;
                gResult =true; 
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",TO";  
                endCallBack = true;            
             }
   }
   return content;
}

String Command(char *cmd)
{  
   #if DEBUG
   Serial.println(*cmd);
   #endif
   String resp;
   int code = (int)*cmd;
   if(code > 63 && code<71 ) // set command A - F
   {
      int count = 0;
      String value; 
      cmd++;
      while(45<*cmd&&*cmd<123)
      {     
        value+=*cmd;
        cmd++;
        count++;
      }
      #if DEBUG
      Serial.println("V="+value);
      #endif
      if(code == 65) // http://{ip}/@A 
      {
        // Set server url
        #if DEBUG
         Serial.println(65);Serial.println(count);     
         #endif
         if(count<21)
          {
              return "SU";     
          }
           return "FC";  
      }
      else if(code == 66) //B http://{ip}/@B
      {
        #if DEBUG
        Serial.println(66);Serial.println(count);
        #endif
          //Set API sKey
          if(count<6)
          {
             // value.toCharArray(APIKEY,count+1);
             //Serial.println(APIKEY);
             return "SU";   
          }
         return "FC";   
             
      }
   }else if(code > 70 && code<79){//G-N
        if(code==71) //G Set led @G0 0=close ,1=open
        {   
          cmd++;
          if(*cmd=='1'){
           gleds[0] = true;
           digitalWrite(LEDTEST, HIGH);
          }else{
           gleds[0] = false;
           digitalWrite(LEDTEST, LOW);
          }
          return "OK";
        }else if(code == 72) //H Get url ,APK , Name
        {                    
         /* resp =  ServerUrl;  
          resp += ",";
          resp +=APIKEY; */
        }else if(code== 73) //I Get status led
        {
            resp="S:";
           int count = sizeof(gleds);
           for(int i=0;i<count;i++)
           {
              resp+=i;
              resp+=(gleds[i])? "T":"F" ;
              resp+=",";
           }
           return resp;
        }
      }
   
   return "NF";
}

void Responce(const int connectionId, String *webpage) 
{
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        //HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage->length();
        HeaderContent += "\r\n\r\n"+*webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        GetAPResponceArray(cipSend,500,">",1);  
        GetAPResponceArray(HeaderContent,1000,"OK",2);    
        String closeCommand = "AT+CIPCLOSE="; 
        closeCommand+=connectionId; // append connection id
        closeCommand+="\r\n";
        GetAPResponceArray(closeCommand,500,"OK",2);    
      
}

String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       delay(10);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                #if DEBUG
                Serial.println("---ack---");
                #endif
                endCallBack = true;  
                gResult =true;  
                return content;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
      
                endCallBack= true;
                gResult =true; 
                return content;
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                
                endCallBack= true;
                return content;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                
                endCallBack= true;
                return content;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;     
                return content;       
             }
   }
   return content;
}

void Interval()
{
  //It should use flash becuase laste mem alive in the sram.
  //Discuss more,this function use stack memory that laste memory used it do't has removed.  
  livingInterval = true;
  gCallServer = true;
}
void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() >0 ) {
         delay(10);
         esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}

//Refence http://forum.arduino.cc/index.php?topic=37470.0
void EEPROMWriteInt(int p_address, int p_value)
     {
     byte lowByte = ((p_value >> 0) & 0xFF);
     byte highByte = ((p_value >> 8) & 0xFF);

     EEPROM.write(p_address, lowByte);
     EEPROM.write(p_address + 1, highByte);
     }

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int EEPROMReadInt(int p_address)
     {
     byte lowByte = EEPROM.read(p_address);
     byte highByte = EEPROM.read(p_address + 1);

     return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
     }
     

