#include <SoftwareSerial.h>
#include "Timer.h"
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 64

char buffer[BUFFER_SIZE];
/*char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/

bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false;
//String http_req;
Timer t;

String GetAPResponceArray(const int timeout, const char* ack,int len = 0);

void setup() {
  Serial.begin(38400);
  esp8266.begin(38400); 
  pinMode(13, OUTPUT);
  //digitalWrite(13, open);
  ResetConfigBase();
  t.every(10000, Interval);
}

void loop() {
  
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else  if(esp8266.available()) // check if the esp is sending a message 
  {  
    clearBuffer();
   
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    Serial.println(buffer);
    
    if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0||strncmp(buffer, "+CIPMUX:0", 9)==0)
    { 
      WIFINEEDREST =true;
      //ResetConfigBase();
    }else if(strncmp(buffer, "+IPD,", 5)==0){
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
        char *pb;    
        //Serial.println("---REQ---");
        String webpage="NF";         
        pb = buffer+5;        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        //Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+";
        int code = (int)*pb;        
        if(64==code)//@ command
        {
          //Serial.println("--GETCMD--");    
          
          webpage = "C"+Command(++pb); 
          clearWifBuffer();
        }else if(code>66 ){         
              //Serial.print("--QUEUEY--");
              clearWifBuffer();
              switch(code)
              {
                /*
                We do not need it.
                case 71: //G List AP
                ComEsp8266+="CWLAP";
                break;*/
                case 67://C Check current AP
                ComEsp8266+="CWJAP?";
                break;
                           
                case 73://I Get IP
                ComEsp8266+="CIFSR";
                break;
              }
              
              esp8266.println(ComEsp8266);
              //webpage ="Q"+ GetAPResponceArray(10000);
              webpage = "Q"+GetAPResponceArray(10000,"OK",2);
              //webpage = "{r:['Q',"+ GetAPResponceArray(ComEsp8266,10000,"O",1)+"]}";
             
        }
        else if(78==code){
        //N Get name Itseft.       
        }
        else if( 67>code && 64<code) //A Join AP,B set name passs
        {
            clearWifBuffer();
           
            String value[2]; 
            int index = 0;
            
            while(*pb!=61) pb++;  
            //Serial.println( *pb);
            pb++;
            
            while(*pb > 45 && *pb<122 && index <2)
            {
              value[index]+=*pb;
             
              if(*(pb+1)==44)
              {               
                index++;
                pb++;
              }
               pb++;
            }
           if(65==code)//A Join AP
           {
                 ComEsp8266 += "CWJAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
              
           }else if(66== code)//Set name ap.
           {
                 ComEsp8266 += "CWSAP=";
                 ComEsp8266 +='"'+ value[0]+'"'+','+'"'+ value[1]+'"';   
                 ComEsp8266 +=",10,0";
           }  
           //Serial.println(ComEsp8266);
           esp8266.println(ComEsp8266+"\0");
           webpage = "S"+GetAPResponceArray(30000,"OK",2);
        }   
         //Serial.println("------------Web-------------");
         //Serial.println(webpage);
         //Serial.println("---------------------------");
         Responce(connectionId,&webpage);        
    }
  }
  else t.update();
 
 /*if( ul_CurrentMillis - ul_PreviousMillis > ul_Interval)
  {      
         requestGetTest();
        //Serial.println("---checking---");
        esp8266.println("AT+CIPMUX?");
        ul_PreviousMillis = millis();
        //cureentTime =millis();
         //lWaitMillis += 10000;
  } */
    
  /*if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }*/
}
void Interval()
{
  livingInterval = true;
  //Serial.println("---checking---");
  requestGetTest();
  esp8266.println("AT+CIPMUX?");
  livingInterval = false;
}
String GetAPResponceArray(const int timeout, const char* ack,int len )
{   
  bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       //clearBuffer();
       //delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             Serial.println(buffer);
             content=content+","+buffer+"";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                //Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",TO";  
                endCallBack = true;            
             }
   }
   return content;
}

String Command(char *cmd)
{  
   Serial.println(*cmd);
   String resp;
   int code = (int)*cmd;
   if(code > 63 && code<71 ) // set command A - F
   {
      int count = 0;
      String value; 
      cmd++;
      while(45<*cmd&&*cmd<123)
      {     
        value+=*cmd;
        cmd++;
        count++;
      }
      Serial.println("V="+value);
      if(code == 65) // A 
      {
        // Set server url
         Serial.println(65);Serial.println(count);     
         if(count<21)
          {
             //value.toCharArray(ServerUrl,count+1);
             // Serial.println(ServerUrl);
              return "SU";     
          }
           return "FC";  
      }
      else if(code == 66) //B
      {
        Serial.println(66);Serial.println(count);
          //Set API sKey
          if(count<6)
          {
             // value.toCharArray(APIKEY,count+1);
             //Serial.println(APIKEY);
             return "SU";   
          }
         return "FC";   
             
      }
   }else if(code > 70 && code<79){//G-N
        if(code==71) //G Get url ,APK , Name
        {   
          
         /* resp =  ServerUrl;  
          resp += ",";
          resp +=APIKEY; */
          return resp;
        }
      }
   
   return "NF";
}

void Responce(const int connectionId, String *webpage) 
{
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        //HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage->length();
        HeaderContent += "\r\n\r\n"+*webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        //delay(1000); 
        //Serial.println(GetAPResponceArray(cipSend,500,">",1)); 
        //Serial.println("----CIPSEND-----");
        //esp8266.println(cipSend);
        GetAPResponceArray(cipSend,500,">",1);  
        //delay(1000);       
        //Serial.println("---body---");
        //esp8266.println(HeaderContent+"\0");
        Serial.println(GetAPResponceArray(HeaderContent,1000,"OK",2));    
        //Serial.println("---close---");
        String closeCommand = "AT+CIPCLOSE="; 
        closeCommand+=connectionId; // append connection id
        closeCommand+="\r\n";
        //esp8266.println(closeCommand+"\0");
        Serial.println(GetAPResponceArray(closeCommand,500,"OK",2));    
        //Serial.println("---END-CLOSE---");
}

String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             Serial.println(buffer);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                //Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;            
             }
   }
   return content;
}
void requestGetTest()
{
  //http://192.168.1.35:8080/emoncms/
  String cmd = "AT+CIPSTART=4,\"TCP\",\"192.168.1.35\",8080";
  gResult = false;
  Serial.println("AT+CIPSTART");
  esp8266.println(cmd);
  GetAPResponceArray(30000,"AL",2);
  
  if(!gResult)
  {
   Serial.println("---Connection fail---");
   return;
  }
  // Create raw HTTP request for web page.
  // "GET status/4 HTTP/1.1\r\nHost: 192.168.1.34: 16290 \r\n\r\n"; 50
  String http_req = "GET /emoncms/ HTTP/1.1\r\nHost: 192.168.1.35:8080\r\n\r\n";
  //http_req += "Content-Type: text/plain\r\n";
  // Ready the module to receive raw data.
  //AT+CIPSEND=4,50
  String cmd2 = "AT+CIPSEND=4,";
   
   cmd2 = cmd2 + http_req.length(); // Tell the ESP8266 how long the coming HTTP request is.
 
  gResult = false;
  esp8266.println(cmd2);
  GetAPResponceArray(30000,">",1);
  if(gResult)
  {    
      esp8266.println(http_req);
      GetAPResponceArray(30000,"OK",2);
  }
  Serial.println("---close---");
  //AT+CIPCLOSE=4
  String closeCommand = "AT+CIPCLOSE=4"; 
  //closeCommand+=connectionId; // append connection id
  closeCommand+="\r\n";
  esp8266.println(closeCommand);     
  GetAPResponceArray(500,"OK",2);   
  Serial.println("---end-close---");
}
void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}

