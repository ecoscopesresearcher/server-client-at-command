#include <SoftwareSerial.h>
#include "Timer.h"
#include "sha1.h"
#include "Base64.h"
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
/*char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/

bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false,WS=false;
Timer t;
String GetAPResponceArray(const int timeout, const char* ack,int len = 0);
void setup() {
  Serial.begin(38400);
  esp8266.begin(38400); 
  pinMode(13, OUTPUT);
  //digitalWrite(13, open);
  ResetConfigBase();  
  t.every(30000, Interval);
}

void loop() {
  //unsigned long int time = millis();
  // put your main code here, to run repeatedly:
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else if(esp8266.available()) // check if the esp is sending a message 
  {  
    //clearBuffer();  
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    Serial.println(buffer);   
     if(strncmp(buffer, "+IPD,", 5)==0){
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
        char *pb;    
        //Serial.println("---REQ---");
        String webpage="NF";         
        pb = buffer+5;        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        //Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+";
        int code = (int)*pb;  
        if(87==code){
           if(esp8266.find("Key: ")){
                String key = "";
                
                while(true) {
                    if(esp8266.available()) {
                      char c = (char)esp8266.read();
                      if(c == '=') {                       
                        doHandshake(key + "==");                        
                        key = "";
                        break;
                      } 
                      if(c != '\r' || c != '\n') {
                        key = key + c;
                      }
                    }
                  }
                  
           }
           while(true) {
                if(Serial.find(":")) { 
                  Serial.println("FRAME");
                }
            }
        }      
      
           
         //Serial.println("------------Web-------------");
         //Serial.println(webpage);
         //Serial.println("---------------------------");
         
    }
  }
   else  if(livingInterval)
  {
    esp8266.println("AT+CIPMUX?");
    livingInterval = false;
  }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0||strncmp(buffer, "+CIPMUX:0", 9)==0)
  { 
      WIFINEEDREST =true;
  }
  //else t.update();
    
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }
}

bool doHandshake(String k) {
  clearWifBuffer();
    Serial.println("do handshake: " + k);
    char bite;
    char temp[128];
    char key[80];
    memset(temp, '\0', sizeof(temp));
    memset(key, '\0', sizeof(key));
     Serial.println("end mem");
    byte counter = 0;
    int myCo = 0;
    while ((bite = k.charAt(myCo++)) != 0) {
      key[counter++] = bite;
    }
    Serial.print(key);
    strcat(key, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"); // Add the omni-valid GUID
    Sha1.init();
    Sha1.print(key);
    Serial.println("########");
    uint8_t *hash = Sha1.result();
    
    base64_encode(temp, (char*)hash, 20);
    
    Serial.println("base64_encode");Serial.println(temp);
    
    int cc = -1;
    while(temp[cc++] != '\0') {} // cc is length return key
    //cc = 165 + cc; // length return key + 165 keys for rest of header
   String HeaderContent;
   HeaderContent =  "HTTP/1.1 101 Switching Protocols\r\n";       
   HeaderContent += "Upgrade: websocket\r\n";   
   HeaderContent += "Connection: Upgrade\r\n";
   HeaderContent += "Sec-WebSocket-Accept: ";
   /*HeaderContent += temp;*/
   //HeaderContent +="\r\n\r\n";
    String commd = "AT+CIPSEND=0,";
    int co = (HeaderContent.length()+cc);
     Serial.println(co); 
    commd+=co;
    commd +="\r\n";
    //esp8266.print(commd);
    Serial.println(commd); 
    boolean found = false;
    GetAPResponceArray(commd,500,">",1);  
    Serial.println("SEND");  
    esp8266.print("HTTP/1.1 101 Switching Protocols\r\n");
    esp8266.print("Upgrade: websocket\r\n");
    esp8266.print("Connection: Upgrade\r\n");
    esp8266.print("Sec-WebSocket-Accept: ");
    esp8266.print(temp);
    esp8266.print("\r\n\r\n");
    Serial.println("END");  
    return true;
}

String GetAPResponceArray(const int timeout, const char* ack,int len )
{   
  bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             Serial.println(buffer);
             content=content+","+buffer+"";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                //Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",TO";  
                endCallBack = true;            
             }
   }
   return content;
}

String Command(char *cmd)
{  
   Serial.println(*cmd);
   String resp;
   int code = (int)*cmd;
   if(code > 63 && code<71 ) // set command A - F
   {
      int count = 0;
      String value; 
      cmd++;
      while(45<*cmd&&*cmd<123)
      {     
        value+=*cmd;
        cmd++;
        count++;
      }
      Serial.println("V="+value);
      if(code == 65) // A 
      {
        // Set server url
         Serial.println(65);Serial.println(count);     
         if(count<21)
          {
             //value.toCharArray(ServerUrl,count+1);
             // Serial.println(ServerUrl);
              return "SU";     
          }
           return "FC";  
      }
      else if(code == 66) //B
      {
        Serial.println(66);Serial.println(count);
          //Set API sKey
          if(count<6)
          {
             // value.toCharArray(APIKEY,count+1);
             //Serial.println(APIKEY);
             return "SU";   
          }
         return "FC";   
             
      }
   }else if(code > 70 && code<79){//G-N
        if(code==71) //G Get url ,APK , Name
        {   
          
         /* resp =  ServerUrl;  
          resp += ",";
          resp +=APIKEY; */
          return resp;
        }
      }
   
   return "NF";
}

void Responce(const int connectionId, String *webpage) 
{
        String HeaderContent;
        HeaderContent =  "HTTP/1.1 200 OK\r\n";
        //HeaderContent += "Content-Type: application/json\r\n";
        HeaderContent += "Connection: close\r\n";   
        HeaderContent += "Content-Length: ";
        HeaderContent += webpage->length();
        HeaderContent += "\r\n\r\n"+*webpage;
        String cipSend = "AT+CIPSEND=";
        cipSend += connectionId;
        cipSend += ",";
        cipSend +=HeaderContent.length();
        cipSend +="\r\n";
        //delay(1000); 
        //Serial.println(GetAPResponceArray(cipSend,500,">",1)); 
        //Serial.println("----CIPSEND-----");
        //esp8266.println(cipSend);
        GetAPResponceArray(cipSend,500,">",1);  
        //delay(1000);       
        //Serial.println("---body---");
        //esp8266.println(HeaderContent+"\0");
        Serial.println(GetAPResponceArray(HeaderContent,1000,"OK",2));    
        //Serial.println("---close---");
        String closeCommand = "AT+CIPCLOSE="; 
        closeCommand+=connectionId; // append connection id
        closeCommand+="\r\n";
        //esp8266.println(closeCommand+"\0");
        Serial.println(GetAPResponceArray(closeCommand,500,"OK",2));    
        //Serial.println("---END-CLOSE---");
}

String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             //Serial.println(buffer);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;            
             }
   }
   return content;
}

void Interval()
{
  livingInterval = true;
}
void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
