#include <SoftwareSerial.h>
#include "Timer.h"
#include "sha1.h"
#include "Base64.h"
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 128
char buffer[BUFFER_SIZE];
/*char PlugName[15];
char ServerUrl[20];
char APIKEY[5];*/

bool WIFINEEDREST = false,livingInterval=false;
bool gResult = false,WS=false;
Timer t;
String GetAPResponceArray(const int timeout, const char* ack,int len = 0);
void setup() {
  Serial.begin(9600);
  esp8266.begin(9600); 
  pinMode(13, OUTPUT);
  
   //esp8266.println("AT+CIOBAUD=9600"); 
  //digitalWrite(13, open);
  ResetConfigBase();  
  //t.every(30000, Interval);
  //doHandshake("jl16ESdtUX749NPDeXbZeA==");
}

void loop() {
  //unsigned long int time = millis();
  // put your main code here, to run repeatedly:
  
  if(WIFINEEDREST)
  {
    ResetConfigBase();
    WIFINEEDREST =false;
  }
  else if(esp8266.available()) // check if the esp is sending a message 
  {  
    //clearBuffer();  
    esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    if(strncmp(buffer, "Unlink", 6)==0)
    {
      WS=false;
    }
    Serial.println(buffer);   
     if(strncmp(buffer, "+IPD,", 5)==0){
        if(WS){
          Serial.println("Get frame"); 
         return;
        }
        int connectionId,packet_len;
        sscanf(buffer+5, "%d,%d", &connectionId, &packet_len);        
        char *pb;    
        //Serial.println("---REQ---");
        String webpage="NF";         
        pb = buffer+5;        
        while(*pb!=':') pb++;
        pb++;
        pb+=5;
        //Serial.print("ASCII=");Serial.println((int)*pb);
        String ComEsp8266 ="AT+";
        int code = (int)*pb;  
        if(87==code){
           
           if(esp8266.find("Key: ")){
           
                String key = "";
                bool hasSend = true;
                while(hasSend) {
                    if(esp8266.available()) {
                     
                      char c = (char)esp8266.read();
                      if(c == '=') {
                      
                       clearWifBuffer();                     
                       key = doHandshake(key + "==");                             
                       String HeaderContent;
                       HeaderContent =  "HTTP/1.1 101 Switching Protocols Handshake\r\n";
                       HeaderContent += "Upgrade: websocket\r\n";
                       HeaderContent += "Connection: Upgrade\r\n";
                       HeaderContent +="Sec-WebSocket-Accept: ";
                       HeaderContent +=key;    
                       HeaderContent +="\r\n\r\n";                      
                       //HeaderContent += "Access-Control-Allow-Credentials: true\r\n";
                       //HeaderContent += "Sec-WebSocket-Origin: chrome-extension://hgmloofddffdnphfgcellkdfbfbjeloo\r\n";
                       //HeaderContent += "Sec-WebSocket-Origin: ws://192.168.1.35/W\r\n" ;
                       /*String HeaderContent2 ="Sec-WebSocket-Accept: ";
                       HeaderContent2 +=key; 
                       HeaderContent2 +="\r\n\r\n";*/
                       String commd = "AT+CIPSEND=0,";
                       commd+= (HeaderContent.length());
                       commd+="\r\n";
                        Serial.print("key=");Serial.println(key);
                        Serial.println(commd);
                        Serial.println(HeaderContent);
                        //Serial.println(HeaderContent2);
                        GetAPResponceArray(commd,1500,">",1);                   
                          
                         esp8266.println(HeaderContent);
                         //esp8266.println(HeaderContent2);
                         //esp8266.println("\r\n\r\n");
                       //GetAPResponceArray(HeaderContent,1000,"OK",2);
                       Serial.println("------END------");
                       key = "";
                       hasSend = false;WS=true;
                       return;
                      } 
                      if(c != '\r' || c != '\n') {
                        key = key + c;
                      }
                    }
                  }
                  
           }
           while(true) {
                if(Serial.find(":")) { 
                   Serial.println("FRAME");
                }
            }
        }      
      
           
         //Serial.println("------------Web-------------");
         //Serial.println(webpage);
         //Serial.println("---------------------------");
         
    }
  }
   else  if(livingInterval)
  {
    esp8266.println("AT+CIPMUX?");
    livingInterval = false;
  }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0||strncmp(buffer, "+CIPMUX:0", 9)==0)
  { 
      WIFINEEDREST =true;
  }
  //else t.update();
    
  if(Serial.available())
  {
    delay(500);     
    String command="";
    while(Serial.available()) // read the command character by character
    {
      //read one character
      command+=(char)Serial.read();
    }   
     esp8266.println(command); // send the read character to the esp8266 

    if(command.substring(0,2)=="AT")
    {
       
    }  
  }
}

String doHandshake(String newkey) {
    
    //Serial.println(newkey);

    newkey+="258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    //Serial.println(newkey);
     uint8_t *hash;
     char result[21];
     char b64Result[30];

     Sha1.init();
     Sha1.print(newkey);
     hash = Sha1.result();
     for (int i=0; i<20; ++i) {
                result[i] = (char)hash[i];
     }
      result[20] = '\0';
      for (int i=0; i<20; ++i) {
      //Serial.println((uint8_t)result[i]);
      }
      base64_encode(b64Result, result, 20);
      //Serial.println(b64Result);
      return b64Result;
}

String GetAPResponceArray(const int timeout, const char* ack,int len )
{   
  bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             Serial.println(buffer);
             content=content+","+buffer+"";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                //Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",TO";  
                endCallBack = true;            
             }
   }
   return content;
}




String GetAPResponceArray(String command,const int timeout, const char* ack,int len)
{
   esp8266.print(command+"\0");
   bool endCallBack = false;
   String content="";
   long int time = millis();        
   while(!endCallBack){
       clearBuffer();
       delay(100);
       if(esp8266.available()){
             esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
             //Serial.println(buffer);
             content=content+",'"+buffer+"'";       
             if(len!=0&&strncmp(buffer, ack,len)==0){
                Serial.println("---ack---");
                endCallBack = true;  
                gResult =true;  
             }else if(strncmp(buffer, "busy", 4)==0)
             {
                WIFINEEDREST = true;
                //Serial.println("---busy---");
                endCallBack= true;
             }else if(strncmp(buffer, "ready", 5)==0||strncmp(buffer, "MUX=0", 5)==0){
                WIFINEEDREST = true;
                //Serial.println("---reay---");
                endCallBack= true;
             }else if(strncmp(buffer, "OK", 2)==0||strncmp(buffer, "ERROR", 5)==0){
                //Serial.println("---list---");
                endCallBack= true;
             }
       }else if((time+timeout) < millis())
             {
                content=content+",'TO'";  
                endCallBack = true;            
             }
   }
   return content;
}

void Interval()
{
  livingInterval = true;
}
void ResetConfigBase()
{
    /*esp8266.println("AT+RST");
    GetAPResponceArray(1000);*/
    esp8266.println("AT+CWMODE=3");
    delay(500);  
    esp8266.println("AT+CIFSR");
    delay(500);   
    esp8266.println("AT+CIPMUX=1");
    delay(500); 
    esp8266.println("AT+CIPSERVER=1,80");     
}
void clearWifBuffer(void) {
      
       while ( esp8266.available() > 0 ) {
         delay(100);
         esp8266.read();
       }
}

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
