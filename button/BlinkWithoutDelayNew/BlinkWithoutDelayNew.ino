#define BUTTON_PIN        7  // Button

#define LONGPRESS_LEN    25  // Min nr of loops for a long press
#define DELAY            20  // Delay per loop in ms

enum States { EV_NONE=0, EV_SHORTPRESS =1, EV_LONGPRESS=2};

boolean button_was_pressed; // previous state
int button_pressed_counter; // press running duration

const int ledPin =  10;  
int ledState = LOW; 

const int deviceControlPin =  13; 
int deviceControlState = LOW;

unsigned long previousMillis = 0;
const long interval = 500;

boolean led_open;
boolean led_blink;

void setup()
{
  pinMode(ledPin, OUTPUT);
  pinMode(deviceControlPin, OUTPUT);
  
  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(BUTTON_PIN, LOW); // pull-up
  Serial.begin(9600);
  button_was_pressed = false;
  button_pressed_counter = 0;
  led_open = false;
  led_blink = false;
}

void blink_led(boolean open,boolean blink){
  if(open){
    if(blink){
         unsigned long currentMillis = millis();
 
          if(currentMillis - previousMillis >= interval) {
            // save the last time you blinked the LED 
            previousMillis = currentMillis;   
        
            // if the LED is off turn it on and vice-versa:
            if (ledState == LOW)
              ledState = HIGH;
            else
              ledState = LOW;
          }
    }else{
      ledState=HIGH;
    }
  }else{
    ledState = LOW;
  }
  digitalWrite(ledPin, ledState);
}

void OpenDevice(){
  digitalWrite(deviceControlPin, deviceControlState);
}

int handle_button()
{
  int event;
  int button_now_pressed = digitalRead(BUTTON_PIN); // pin low -> pressed
  //Serial.print(button_now_pressed);
  
  if (!button_now_pressed && button_was_pressed) {
    if (button_pressed_counter < 20){
      event = EV_SHORTPRESS ;
    }
  }
  else
    event = EV_NONE;
  
  if (button_now_pressed){
    button_pressed_counter= button_pressed_counter+1;
    if(button_pressed_counter > 80)
    event = EV_LONGPRESS;
   // Serial.print('<>'+button_pressed_counter);
  }
  else{
    button_pressed_counter = 0;
  }

  button_was_pressed = button_now_pressed;
  return event;
}

void loop()
{
  // handle button
  int event = handle_button();

  // do other things
  switch (event) {
    case EV_NONE:
      break;
    case EV_SHORTPRESS:
      deviceControlState = !deviceControlState;
      Serial.println("EV_SHORTPRESS");
      break;
    case EV_LONGPRESS:
      led_open = true;
      led_blink = true;
      Serial.println("EV_LONGPRESS");
      break;
  }

 blink_led(led_open , led_blink);
 //digitalWrite(ledPin, ledState);
  // add newline sometimes
//  static int counter = 0;
 // if ((++counter & 0x3f) == 0)
 //   Serial.println();
OpenDevice();
  delay(DELAY);
}

